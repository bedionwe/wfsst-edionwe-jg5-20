﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PiCalculation
{
    class Program
    {
        static void Main(string[] args)
        {
            double count;
            Console.WriteLine("Geben Sie eine Anzahl der Punkte zur Berechnung von Pi ein");
            try
            {
                count = double.Parse(Console.ReadLine());
            }
            catch
            {
                Console.WriteLine("Bitte nur ganzzahlige Werte eingeben");
                count = double.Parse(Console.ReadLine()); 
            }



            InsideRectangle(count);
            Console.ReadKey();
            
        }

        public static void InsideRectangle(double count)
        {
            Random r = new Random();
            

            double inside = 0;
            double radius;
            double pi; 

            for (int i = 0; i < count; i++)
            {
                double x = r.NextDouble();
                double y = r.NextDouble();

                radius = Math.Sqrt(Math.Pow(x, 2) + Math.Pow(y, 2));
                Console.WriteLine(radius);
                

                if (radius <= 1)
                    inside++;        


            }

            pi = inside / count * 4;
            Console.WriteLine($"Das berechnete Pi lautet {pi} tatsächlicher Wert 3.141592");
            


        } 

    }
}
