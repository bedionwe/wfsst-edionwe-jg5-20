﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiThreadingIntro
{
    class Program
    {
        const int MAX = 16;
        const int DELTA = 4;
        const int MIN = 0;
        const int THREADCOUNT = (MAX - MIN) / DELTA;
        static void Main(string[] args)
        {
            Stopwatch stopWatch = new Stopwatch();
            Console.BufferHeight = 1000;
            int result = 0;

            #region Single Threaded
            Console.WriteLine($"Summe von {MIN} bis {MAX} in einem Thread");
            stopWatch.Restart();
            result = 0;
            result = Sum(MIN, MAX, 0);
            Console.WriteLine($"Summe von {MIN} bis {MAX} = {result}");
            Console.WriteLine($"Dauer Singlethreaded: {stopWatch.ElapsedMilliseconds: #,##0}ms");
            Console.WriteLine("Weiter mit <cr>\n");
            Console.ReadLine();
            Console.Clear();
            #endregion

            #region Multithreaded without LOCK 
            /*Unlocked: Treads laufen nicht der Reihe nach ab bis zum einde des jeweiligen Threads 
             * Die Threads werden zwar der Reihe nach gestartet aber die Abarbeitung erfolg scheinbar chaotisch
             * 
             * Anwendung dann wenn die Threads unabhängig voneinander arbeiten dürfen 
             */
            Console.WriteLine($"Summe von {MIN} bis {MAX} in {THREADCOUNT} unlocked Threads");
            result = 0;
            stopWatch.Restart();
            Parallel.For(0, THREADCOUNT, (index) =>  //Hilfe die automatishe ein paar Threads startet
             {
                 int intermed = Sum(index * DELTA, (index + 1) * DELTA, index);
                 result += intermed;
             });
            result = Sum(MIN, MAX, 0);
            Console.WriteLine($"Summe von {MIN} bis {MAX} = {result}");
            Console.WriteLine($"Dauer Singlethreaded: {stopWatch.ElapsedMilliseconds: #,##0}ms");
            Console.WriteLine("Weiter mit <cr>\n");
            Console.ReadLine();
            Console.Clear();
            #endregion

            #region Multi Threading with LOCK
            Console.WriteLine($"Summe von {MIN} bis {MAX} in {THREADCOUNT} locked Threads");
            result = 0;
            Object lockObj = new Object();
            Parallel.For(0, THREADCOUNT, (index) =>  //Hilfe die automatishe ein paar Threads startet
            {
                lock (lockObj)
                {
                    int intermed = Sum(index * DELTA, (index + 1) * DELTA, index);
                    result += intermed; 
                }
            });
            stopWatch.Restart();
            result = Sum(MIN, MAX, 0);
            Console.WriteLine($"Summe von {MIN} bis {MAX} = {result}");
            Console.WriteLine($"Dauer Singlethreaded: {stopWatch.ElapsedMilliseconds: #,##0}ms");
            Console.WriteLine("Weiter mit <cr>\n");
            Console.ReadLine();
            Console.Clear();
            #endregion
        }
        public static int Sum(int lower, int upper, int threadNum)
        {
            int result = 0;
            double dummy = 0.0;

            for (int i = lower; i < upper; i++)
            {
                result += i;
                //Quatschrechnung um Zeit zu schinden, und die CPU zu quälen 
                for (int b = 0; b < 1e6; b++)
                {
                    dummy = Math.Pow(b, Math.Log(b));
                }
                Console.WriteLine($"Thread {threadNum: 000} Processing...");
            }

            Console.WriteLine($"Thread {threadNum: 000} result: [{lower,5};{upper,5} = {result,5}");
            return result;
        }
    }
}
