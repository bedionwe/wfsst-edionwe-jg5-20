
# Aufgabenstellung
Die aus der Corona Todesverteilung entnommenen Daten, sollen in einem 3D-Balkendiagramm
mittels Helix-Toolkit dargestellt werden. Dabei soll mittels Combobox ein Element gewählt werden (Altersgruppe) 
Diese soll dann farblich von den anderen abgehobn werden. Es handelt sich hierbei um eine Erweiterung eines Programms, dass
in der Klasse gemeinam geschrieben wurde. 

## Vorgangsweise bei der Programmierung...

Verwendet wurde BoxVisual3D zur Realisierung der Balken, dabei handelt es sich um einen Quader, welcher ins ViewPort hinzugefügt werden kann. 
Bestimmt werden dabei folgende relevante Parameter
- Länge
- Breie 
- Höhe
- Position des Mittelpunkts
- Farbe 

~~~csharp
using System;

namespace WhatsUpClonModel
{
    public class WatchDogEventArgs : EventArgs
    {
        public uint Pings { get; set; }
        public TimeSpan Uptime { get; set; }
        public bool IsUnavailable { get; set; }
        public string Message { get; set; }
    }
}

~~~

Die Länge und die Breite sollen für alle Balken gleich sein (Grundfläche), 
die Höhe soll durch Binding an den jeweiligen Balken weitergegeben werden. 
Auch die Position, und die Farbe sollen mittels Binding bestimmt werden. 

## Binding Höhe
Ins VM der List wurde ein neues Property erstellt, es handelt sich dabei um eine Liste, 
die die Höhen aller Balken enthält, darin werden die Höhen um einen Faktor 15 Verringert. 



Binding Position
Auch hier wird ein neues Property angelgt es handelt sich um eine 3D-PointCollection. 
Was hier entcheidend ist, ist das die Position der Balken auf eine Ebene gerechnet werden muss, 
da immer nur die Position der Mitte angegeben werden kann, muss die richtige Position je nach Höhe umgerechnet werden. 

Binding Farbe
Es wird im VM der Liste ien Property angelegt, welches immer dann aktualisiert wird, wenn sich das SelectedItem ändert. (OnPropertyChanged)
Es handelt sich bei dem Property um eine Liste aus SolidColorBrush-Elementen, Im Property wird die gesamte Liste durchgegangen und überprüft, ob die Altersgruppe des ausgewählten Elemnts mit einer Altersgruppe der List übereinstimmt, wenn ja wird an dieser Stelle der List die Farbe verändert. 


Die Einzelnen Listenelemente werden jetzt mit den einzelnen Balken verbunden. z.B Height="{Binding BoxHeigt[0]}"
