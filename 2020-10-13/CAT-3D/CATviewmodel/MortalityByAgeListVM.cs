﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using CATmodel;
namespace CATviewmodel
{
    public class MortalityByAgeListVM : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propname)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propname));
        }
        public MortalityByAgeList Items { get; private set; }

        public MortalityByAgeListVM(string filename)
        {
            Items = MortalityByAgeList.ReadFile(filename);
        }

        private MortalityByAgeModel selectedItem;

        public MortalityByAgeModel SelectedItem
        {
            get { return selectedItem; }
            set
            {
                selectedItem = value;
                foreach (var item in Items)
                {
                    item.Fill = new SolidColorBrush(Colors.Black);
                }
                selectedItem.Fill = new SolidColorBrush(Colors.Red);
                OnPropertyChanged("SelectedItem");
                OnPropertyChanged("SelectedItemX");
                OnPropertyChanged("SelectedItemFill");
            }
        }

        public List<double> BoxHeigt
        {
            get
            {
                List<double> heights = new List<double>();
                
                foreach (var item in Items)
                {
                    heights.Add(item.Count/15);
                    
                }
                return heights;
            }
            
        }

        public Point3DCollection Points
        {

            
            get
            {
                Point3DCollection pc3d = new Point3DCollection(Items.Count);
                double x = 8;
                double z;
                double y=0;
                foreach (var item in Items)
                {
                    z = (item.Count/15)/ 2 - 10;//Fehler im System, Werte werden nicht richtig eingestellt
                    
                    
                    pc3d.Add(new System.Windows.Media.Media3D.Point3D(x, y, z));
                    x = x - 1;
                }
                pc3d[6] = new Point3D(2, 0, -9.5);
               return pc3d;

                //PointCollection pc = new PointCollection(Items.Count);
                //double xx= 0.0;
                //foreach (var item in Items)
                //{
                //    pc.Add(new System.Windows.Point(x, item.Count));
                //    x += 20;
                //}
                //return pc;
            }
        }
        public double SelectedItemX
        {
            get
            {
                if (SelectedItem == null)
                    return 0;
                double x = 0;
                foreach (var item in Items)
                {
                    if (item.Age == selectedItem.Age)
                        break;
                    x += 20;
                }
                return x;
            }
        }

        public List<SolidColorBrush> SelectedItemFill
        {
            get
            {
                List<SolidColorBrush> colList = new List<SolidColorBrush>(Items.Count);
                SolidColorBrush col;
                int i = 0;
                foreach (var item in Items)
                {
                    if (selectedItem == null)
                    {
                        col = new SolidColorBrush(Colors.Blue);
                        break;
                    }
                        
                    if (item.Age == selectedItem.Age)
                        col = new SolidColorBrush(Colors.Violet);
                    else
                        col = new SolidColorBrush(Colors.Blue);
                   // item.Fill = col;
                    colList.Add(col);
                    i++;
                    
                }
                return colList;
            }
        }
       


    }
}
