﻿using CAVTmodel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CAVTviewModel
{
    public class MortalityByAgeVM:INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propname)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propname));


            //var propchanged = PropertyChanged;
            //if (PropertyChanged != null)
            //{
            //    PropertyChanged(this, new PropertyChangedEventArgs(propname));
            //


        
        
        }

        private MortalityByAge item;
        public string Age
        {
            set { item.Age = value;
                OnPropertyChanged("Age");
            }

            get { return item.Age; }

        }

        public int Count
        {
            set
            {
                item.Count = value;
                OnPropertyChanged("Count");
            }

            get { return item.Count; }

        }

        public DateTime Timestamp
        {
            set
            {
                item.Timestamp = value;
                OnPropertyChanged("Timestamp");
            }

            get { return item.Timestamp; }

        }
        public MortalityByAgeVM(string age, int count, DateTime timestamp)
        {
            Age = age;
            Count = count;
            Timestamp = timestamp; 
        }

    }
}
