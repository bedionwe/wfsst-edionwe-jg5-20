﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Hosting;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace StartProcess_StdIn_StdOut_StdErr
{
    /// <summary>
    /// Interaktionslogik für Window1.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Process proc;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void CommandGo(object sender, RoutedEventArgs e)
        {
            proc = new Process();
            proc.StartInfo.FileName = command.Text;
            proc.StartInfo.Arguments = arguments.Text;
            proc.StartInfo.RedirectStandardOutput = true;
            proc.StartInfo.RedirectStandardError = true;
            proc.StartInfo.RedirectStandardInput = true;
            proc.StartInfo.UseShellExecute = false;
            proc.StartInfo.CreateNoWindow = true;
            //richtige Codierung ermitteln mit mode.exe 
            //Standartmäßig meist: Codepage: 850
            proc.StartInfo.StandardOutputEncoding = Encoding.GetEncoding(850);
            proc.StartInfo.StandardErrorEncoding = Encoding.GetEncoding(850);

            //#region Kommunikation mit PRozess nicht über Ereignisse
            //proc.Start();
            //stdOut.Text = proc.StandardOutput.ReadToEnd();
            //stdErr.Text = proc.StandardError.ReadToEnd();
            //#endregion

            #region Kommunikation mit Prozess über Ereignisse
            proc.EnableRaisingEvents = true;
            proc.Exited += Proc_Exited;
            proc.ErrorDataReceived += Proc_ErrorDataReceived;
            proc.OutputDataReceived += Proc_OutputDataReceived;

            proc.Start();
            proc.BeginOutputReadLine();
            proc.BeginErrorReadLine();
            #endregion
        }

        private void Proc_ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            UpdateTextBox(stdErr, e.Data);
        }

        private void Proc_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            UpdateTextBox(stdOut, e.Data);
        }

        

        private void Proc_Exited(object sender, EventArgs e)
        {
            UpdateTextBox(stdOut, $"Prozess beendet mit Exit Code {proc.ExitCode}");
        }

        private void SendStdIn(object sender, RoutedEventArgs e)
        {

            StreamWriter sw = proc.StandardInput;
            { 
                sw.WriteLine(stdIn.Text);
                sw.WriteLine(Convert.ToChar(26)); //26 ist ASCII Code für "Enter"
                sw.Flush();
            }
            
        }

        private void ProcTerminate(object sender, RoutedEventArgs e)
        {
            proc.Kill();
        }

        private void Exit(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void SendStdErr(object sender, RoutedEventArgs e)
        {

        }
        private void UpdateTextBox(TextBox textbox, string message)
        {
            textbox.Dispatcher?.Invoke(new Action(() => textbox.AppendText(message + Environment.NewLine)));

        }
    }
}
