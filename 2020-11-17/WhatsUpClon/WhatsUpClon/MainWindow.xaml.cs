﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WhatsUpClonVM;

namespace WhatsUpClon
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<WatchDogVM> WatchDogVMs = new List<WatchDogVM>();
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            WatchDogVMs.Add(new WatchDogVM(IPAddress.Parse("1.1.1.1")));
            WatchDogVMs.Add(new WatchDogVM(IPAddress.Parse("8.8.8.8")));
            WatchDogVMs.Add(new WatchDogVM(IPAddress.Parse("192.168.178.28")));
            WatchDogVMs.Add(new WatchDogVM(IPAddress.Parse("192.168.178.1")));
            WatchDogVMs.Add(new WatchDogVM(IPAddress.Parse("1.2.3.4")));

            foreach (var item in WatchDogVMs)
            {
                Label l = new Label();
                WrapPanel wrp = new WrapPanel();

                TextBlock tb = new TextBlock() { Style = Resources["Host"] as Style };
                Binding tbBinding = new Binding()
                {
                    Source = item,
                    Path = new PropertyPath("IPAddress")
                };
                BindingOperations.SetBinding(tb, TextBlock.TextProperty, tbBinding);
                wrp.Children.Add(tb);


                Ellipse elli = new Ellipse() { Style = Resources["LED"] as Style };
                Binding elliBinding = new Binding()
                {
                    Source = item,
                    Path = new PropertyPath("SignalColour")
                };
                BindingOperations.SetBinding(elli, Ellipse.FillProperty, elliBinding);
                wrp.Children.Add(elli);


                TextBlock tbUp = new TextBlock() { Style = Resources["Host"] as Style };
                Binding tbUpBinding = new Binding()
                {
                    Source = item,
                    Path = new PropertyPath("UpTime"),
                    StringFormat = "{0:hh}:{0:mm}:{0:ss}"
                };
                BindingOperations.SetBinding(tbUp, TextBlock.TextProperty, tbUpBinding);
                wrp.Children.Add(tbUp);


                TextBlock tbPings = new TextBlock() { Style = Resources["Host"] as Style };
                Binding tbPingsBinding = new Binding()
                {
                    Source = item,
                    Path = new PropertyPath("Pings")
                };
                BindingOperations.SetBinding(tbPings, TextBlock.TextProperty, tbPingsBinding);
                wrp.Children.Add(tbPings);

                TextBlock tbHostname = new TextBlock() { Style = Resources["Hostname"] as Style };
                Binding tbHostnameBinding = new Binding()
                {
                    Source = item,
                    Path = new PropertyPath("Hostname")
                };
                BindingOperations.SetBinding(tbHostname, TextBlock.TextProperty, tbHostnameBinding);
                wrp.Children.Add(tbHostname);

                l.Content = wrp;
                stpMain.Children.Add(l);
            }
        }

    }
}
