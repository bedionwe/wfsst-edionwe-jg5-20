﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using WhatsUpClonModel;


namespace WhatsUpClonVM
{
    public class WatchDogVM: INotifyPropertyChanged, IDisposable
    {
        public string Hostname
        {
            get
            {
                return WatchDog.Hostname;
            }
        }

        private WatchDog WatchDog
        {
            get;
        }

        public IPAddress IPAddress
        {
            get
            {
                return WatchDog.IPAddress;
            }
        }

        public uint Pings
        {
            get
            {
                return WatchDog.Pings;
            }
        }

        public TimeSpan UpTime
        {
            get
            {
                return WatchDog.Uptime;
            }
        }

        public Brush SignalColour
        {
            get
            {
                return WatchDog.IsUnavailable ? Brushes.Red : Brushes.Green;

            }

        }
         
        public WatchDogVM(IPAddress iPAddress)
        {
            WatchDog = new WatchDog(iPAddress);
            WatchDog.Tick += WatchDog_Tick;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }

        private void WatchDog_Tick(object sender, WatchdogEventArgs e)//Was passiert hier
        {
            OnPropertyChanged("Uptime");
            OnPropertyChanged("SignalColor");
            OnPropertyChanged("Pings");
        }

        public void Dispose()
        {
            WatchDog.Process.Kill();
        }
    }
}
