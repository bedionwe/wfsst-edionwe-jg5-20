﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhatsUpClonModel
{
    public class WatchdogEventArgs : EventArgs
       {
        public uint Pings { get; set; }
        public TimeSpan Uptime { get; set; }
        public bool IsUnavailable { get; set; }
        public string Message { get; set; }
        }
}
