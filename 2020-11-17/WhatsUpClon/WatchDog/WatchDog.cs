﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;


namespace WhatsUpClonModel
{
    public class WatchDog
    {

        public event EventHandler<WatchdogEventArgs> Tick;
        private uint pings = 0; 

        public IPAddress IPAddress { get; set; }
        public bool IsUnavailable { get; set; } = true;
        public Process Process { get; set;  }// Was genau ist in diesem Zusammenhang ein Prozess?
        public string Hostname { get; set; }

        public uint Pings
        {
            get
            {
                return pings;
            }
        }
        public DateTime StartedAt { get; }
        public TimeSpan Uptime 
        {
            get
            {
                return (DateTime.Now - StartedAt);
            }
        }
        public WatchDog(IPAddress iPAddress)
        {
            IPAddress = iPAddress;
            Hostname = GetHostname();

            Process = new Process()
            {
                StartInfo = new ProcessStartInfo() //Was passiert hier??
                {
                    FileName = "ping.exe",
                    Arguments = $"/t {IPAddress}",
                    RedirectStandardOutput = true,
                    StandardOutputEncoding = Encoding.GetEncoding(850),
                    UseShellExecute = false,
                    CreateNoWindow = true
                }
            };
            Process.OutputDataReceived += Process_OutputDataReceived;
            Process.Start();
            StartedAt = DateTime.Now;
            Process.BeginOutputReadLine();

        }

        private string GetHostname()
        {
            string hostname;

            try
            {
                hostname = Dns.GetHostEntry(IPAddress).HostName;
            }
            catch (Exception)
            {
                hostname = "not available";
            }
            return hostname;
        }
        private void Process_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(e.Data))
            {
                return;
            }

            pings++;
            WatchdogEventArgs watchDogEventArgs = null;

            if (e.Data.Contains("Zeitüberschreitung"))
            {
                watchDogEventArgs = new WatchdogEventArgs()
                {
                    Pings = pings,
                    IsUnavailable = this.IsUnavailable = true
                };
                
            }
            if (e.Data.Contains("Bytes"))
            {
                watchDogEventArgs = new WatchdogEventArgs()
                {
                    Pings = pings,
                    Uptime = this.Uptime,
                    IsUnavailable = this.IsUnavailable = false
                };
                
            }
            OnTick(watchDogEventArgs);
        }

        protected virtual void OnTick(WatchdogEventArgs watchDogEventArgs)
        {
            Tick?.Invoke(this, watchDogEventArgs);
        }
    }
}
