# Multiple Ping 
## Funktion 
Sinn und Zweck des Programms, ist mehrere Clients zu Überwachen bzw. zu Überprüfen ob diese noch online sind. 

![User Interface](2020-11-17/UserInterface.png)

Das User Interface ist einfach gehalten, grün oder rot zeigen an, ob ein Client erreichbar ist oder nicht. 
Die Zeit, entspricht der vergangenen Zeit seit Verbindungsaufbau. Die Zahl daneben steht dabei für die Anzahl der durchgeführten Pins, während rechts die hostnamen angezeigt werden.
## Umsetzung 
Umgesetzt wurde das Projekt mithilfe der MVVM Variante. 
### WatchDog - Model 
Der Kern des Projekts ist der WatchDog, zu seinen Aufgaben gehören 
- Aufruf der Ping Anwendung über einen externen Prozess, bei seiner Erzeugung 
- Über interne Methode den Hostname der jeweiligen IP-Adresse abfragen
- Überprüfung ob die Pings erfolgreich waren 
- Auslösen eines Events bei jedem Ping 
- Enthält die Zeitaufzeichnung (Zeit die seit erstem Ping vergangen ist)

### WatchDogEventArgs  
Es handelt sich hierbei um ein Event, welches immer dann aufgerufen wird, wenn ein Ping erfolgt ist. Dadurch werden die WatchDog-Propertys aktualisiert. 

### WatchDogVM
Das ViewModel bildet die Verbindung zwischen dem User Interface und dem Model. Die Aufgaben sind wie folgt...
- Definition der Properties
- Prüft auf Änderung der Properties
- Prüft auf WatchDogEvents und aktualisiert gegebene Objekt-Parameter

```csharp
    private void WatchDog_Tick(object sender, WatchdogEventArgs e)//Was passiert hier
        {
            OnPropertyChanged("Uptime");
            OnPropertyChanged("SignalColor");
            OnPropertyChanged("Pings");
        }
```

### Code Behind - ViewModel 
Da es nicht festgelegt ist, wieviele Geräte untersucht werden, müsste für jedes Gerät das gesamte User Interface angepasst werden. Um das zu vermeiden, wird das User Interface im Code Behind angefertigt und abhängig von der Geäteanzahl eigenständig generiert. 

Alle WatchDogs werden in einer Liste gesammelt. Die einzelnen Elemente müssen dann nur einmal hinzugefügt werden. 
```csharp
    WatchDogVMs.Add(new WatchDogVM(IPAddress.Parse("1.1.1.1")));
            WatchDogVMs.Add(new WatchDogVM(IPAddress.Parse("8.8.8.8")));
            WatchDogVMs.Add(new WatchDogVM(IPAddress.Parse("192.168.178.28")));
            WatchDogVMs.Add(new WatchDogVM(IPAddress.Parse("192.168.178.1")));
            WatchDogVMs.Add(new WatchDogVM(IPAddress.Parse("1.2.3.4")));
```
Im Anschluss daran muss für jedes Element der Liste ein eigenes Wrappanel erstellt werden, welches die Elemente richtig anzeigt. 
Dazu wird auch ein Binding benötigt, bei dem Die Element-Properties aus dem User Interface mit den Propertys des jeweiligen WatchDog-Objekts verbundne werden 
```csharp
    Binding tbBinding = new Binding()
                {
                    Source = item,
                    Path = new PropertyPath("IPAddress")
                };
                BindingOperations.SetBinding(tb, TextBlock.TextProperty, tbBinding);
```
Im XAML wurde, außer ein paar Styles zu definieren, nichts gemacht. 


