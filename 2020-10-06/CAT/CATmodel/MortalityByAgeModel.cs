﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CATmodel
{
	public class MortalityByAgeModel
	{
		private int count;
		private string age;
		private DateTime timestamp;

		public DateTime Timestamp
		{
			get { return timestamp; }
			set { timestamp = value; }
		}


		public string Age
		{
			get { return age; }
			set { age = value; }
		}


		public int Count
		{
			get { return count; }
			set { count = value; }
		}

		public MortalityByAgeModel(string age, int anzahl, DateTime timestamp)
		{
			Timestamp = timestamp;
			Age = age;
			Count = anzahl; // korrektur Ruh
			// Count = count;
		}

		public static MortalityByAgeModel Parse(string data)
		{
			string[] tokens = new string[3];
			tokens = data.Split(';');
			return new MortalityByAgeModel(tokens[0], int.Parse(tokens[1]), DateTime.Parse(tokens[2]));
		}
		public MortalityByAgeModel()
		{
		}

	}
}
