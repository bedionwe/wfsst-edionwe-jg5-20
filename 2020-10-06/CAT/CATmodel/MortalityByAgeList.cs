﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CATmodel
{
    public class MortalityByAgeList : List<MortalityByAgeModel>
    {
        
        public static MortalityByAgeList ReadFile(string filename)
        {
            MortalityByAgeList list = new MortalityByAgeList();
            using (StreamReader sr = new StreamReader(filename))
            {
                sr.ReadLine(); //Header lesen (wird nicht benötigt)
                string data = sr.ReadToEnd();
                string[] tokens = data.Split('\n');
                foreach (var item in tokens)
                {
                    list.Add(MortalityByAgeModel.Parse(item));
                }
                //while (sr.EndOfStream)
                //{
                //    list.Add(MortalityByAgeModel.Parse(sr.ReadLine()));
                //}
                
            }
            return list;
        }
    }
}
