﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using CATmodel;
namespace CATviewmodel
{
    public class MortalityByAgeListVM : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propname)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propname));
        }
        public MortalityByAgeList Items { get; private set; }

        public MortalityByAgeListVM(string filename)
        {
            Items = MortalityByAgeList.ReadFile(filename);
        }

        private MortalityByAgeModel selectedItem;

        public MortalityByAgeModel SelectedItem
        {
            get { return selectedItem; }
            set
            {
                selectedItem = value;
                OnPropertyChanged("SelectedItem");
                OnPropertyChanged("SelectedItemX");
            }
        }
        public PointCollection Points
        {
            get
            {
                PointCollection pc = new PointCollection(Items.Count);
                double x = 0.0;
                foreach (var item in Items)
                {
                    pc.Add(new System.Windows.Point(x, item.Count));
                    x += 20;
                }
                return pc;
            }
        }
        public double SelectedItemX
        {
            get
            {
                if (SelectedItem == null)
                    return 0;
                double x = 0;
                foreach (var item in Items)
                {
                    if (item.Age == selectedItem.Age)
                        break;
                    x += 20;
                }
                return x;
            }
        }


    }
}
