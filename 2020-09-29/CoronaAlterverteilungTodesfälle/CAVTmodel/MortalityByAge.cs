﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAVTmodel
{
    public class MortalityByAge
    {
        private int count;
        private string age;
        private DateTime timestamp;

        public DateTime Timestamp
        {
            get { return timestamp; }
            set { timestamp = value; }
        }

        public string Age
        {
            get { return age; }
            set { age = value; }
        }


        public int Count
        {
            get { return count; }
            set { count = value; }
        }

        public MortalityByAge(string age, int count, DateTime timestamp)
        {
            Timestamp = timestamp;
            Count = count;
            Age = age;
        }

        public static MortalityByAge Parse (string data)
        {
            string[] tokens = new string[3];
            tokens = data.Split(';');
            //DateTime dt = new DateTime();
            return new MortalityByAge(tokens[0], int.Parse(tokens[1]), DateTime.Parse(tokens[2]));
        }
    }
}
