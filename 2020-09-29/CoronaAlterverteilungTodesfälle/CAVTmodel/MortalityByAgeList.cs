﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAVTmodel
{
    public class MortalityByAgeList : List<MortalityByAge>
    {
        public static MortalityByAgeList ReadFile(string filename)
        {
            MortalityByAgeList list = new MortalityByAgeList();
            using (StreamReader sr = new StreamReader(filename))
            {
                sr.ReadLine(); //Header einlesen, wird nicht gebraucht

                string data = sr.ReadToEnd();
                string[] tokens = data.Split('\n');
                foreach (var item in tokens)
                {
                    list.Add(MortalityByAge.Parse(item));
                }
                //while (!sr.EndOfStream)
                //{
                //    list.Add(MortalityByAge.Parse(sr.ReadLine()));
                //}



            }
            return list;
        }
    }
}
