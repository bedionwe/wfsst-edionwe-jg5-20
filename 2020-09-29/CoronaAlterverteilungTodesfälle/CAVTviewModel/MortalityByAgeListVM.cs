﻿using CAVTmodel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAVTviewModel
{
    public class MortalityByAgeListVM: INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propname)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propname));
        }

        public MortalityByAgeList Items
        {
            get;
            private set;
        }

        public MortalityByAgeListVM(string filename)
        {
            Items = MortalityByAgeList.ReadFile(filename);
        }

        private MortalityByAge selectedItem;

        public MortalityByAge SelectedItem
        {
            get { return selectedItem; }
            set 
            {
                OnPropertyChanged("SelectedItemX");
                OnPropertyChanged("SelectedItem");
                selectedItem = value; 
            }
        }

        public double SelectedItemX
        {
            get
            {
                if (SelectedItem == null)
                    return 0;

                double x = 0;
                foreach (var item in Items)
                {
                    if (item.Age == SelectedItem.Age)
                        break;
                    x += 20;
                }
                return x; 
            }
        }
    }
}
