﻿using CAVTviewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CoronaAlterverteilungTodesfälleApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        MortalityByAgeListVM items = new MortalityByAgeListVM(@"..\..\AltersverteilungTodesfaelle.csv");//.. heißt eine Ebene nach oben
        public MainWindow()
        {
            
            InitializeComponent();
            cobo.DataContext = items;
            can.DataContext = items; 

        }
    }
}
